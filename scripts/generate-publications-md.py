"""
Simple python script that converts the input file (as a bibtex file) into a zola content file.
"""

import toml
import bibtexparser

md_template = """+++
title = "They use Batsim"
description = "List of Batsim-related publications."
date = 2021-05-25T18:20:00+00:00
updated = 2021-05-25T18:20:00+00:00
draft = false
weight = 421
template = "docs/publications.html"

{publication_list}
+++

# Publication list
"""

data = []
with open("publications.bib") as bibtex_file:
    parser = bibtexparser.bparser.BibTexParser()
    parser.customization = bibtexparser.customization.convert_to_unicode
    bibliography = bibtexparser.load(bibtex_file, parser=parser)
    for entry in bibliography.entries:
        for k, v in entry.items():
            entry[k] = v.replace("\n", " ")

    publication_list = toml.dumps({ "extra": { "list": bibliography.entries } })
    print(md_template.format(publication_list = publication_list))
