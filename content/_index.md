+++
title = ' <img class="img-fluid" src="img/logo.svg" alt="Batsim logo" /> <br />'

# The homepage contents
[extra]
lead = 'Batsim is an <b>infrastructure simulator</b> that enables the study of <b>resource management</b> policies.'
url = "https://batsim.readthedocs.io/en/latest/installation.html"
url_button = "Get started"
repo_version = 'Batsim v4.0.0'
repo_license = 'LGPL 3.0'
repo_url = "https://github.com/oar-team/batsim"

[[extra.list]]
title = "Active community"
content = 'Join us on <a href="https://framateam.org/batsim/channels/town-square">Mattermost</a>, submit issues on <a href="https://github.com/oar-team/batsim/issues">GitHub</a>.'

[[extra.list]]
title = "Tailored for your infrastructure"
content = 'Made for Edge and fog computing, IOT, cloud or HPC systems.'

[[extra.list]]
title = "Tested and maintained"
content = 'See our <a href="https://framagit.org/batsim/batsim/pipelines" rel="nofollow noreferrer noopener" target="_blank"> CI pipelines</a>.'

[[extra.list]]
title = "Sound simulation"
content = 'Batsim is based on <a href="https://simgrid.org/">SimGrid</a> and thus benefits from numerous thorough validation studies.'

[[extra.list]]
title = "Use your favorite language"
content = 'Choose existing schedulers in (<a href="https://gitlab.inria.fr/batsim/pybatsim">Python</a>, <a href="https://gitlab.inria.fr/batsim/datsched">D</a>, or <a href="https://framagit.org/batsim/batsched">C++</a>), or start with your favorite language.'

[[extra.list]]
title = "Build visualization"
content = 'Analyze and visualize results with <a href="https://adrien-faure.fr/blog/gantt-charts/">R</a> or <a href="https://github.com/oar-team/evalys">Python</a>.'

[[extra.list]]
title = "Reproducible"
content = ""
+++

<div class="wrap container" role="document">
  <div class="content">
    <section class="section container-fluid mt-n3 pb-3">
      <div class="row justify-content-center mb-5">
        <div class="col-lg-12 text-center ">
          <h1 class="mt-0">What is Batsim?</h1>
        </div>
      </div>
      <div class="control-group"></div>
      <div class="row justify-content-center">
        <div class="col-lg-6 text-justify">
          <p>
            <b>Batsim</b> is a scientific simulator to analyze batch schedulers. Batch schedulers — or Resource and Jobs Management Systems, RJMSs — are systems that manage resources in large-scale computing centers, notably by scheduling and placing jobs.
          </p>
        </div>
        <div class="col-lg-6 mb-5">
          <p>
            <b>Batsim</b> doesn't take scheduling decisions about when and where the jobs must execute. Instead it delegates these choices to a decision process (typically your scheduler).
          </p>
        </div>
        <div class="col-lg-12">
          <img src="/img/batsim_rjms_overview.svg" class="img-fluid" alt="Batsim overview">
        </div>
      </div>
    </section>
  </div>
</div>
