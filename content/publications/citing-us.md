+++
title = "Citing Batsim"
description = "Contributor Covenant Code of Conduct."
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = ''
toc = false
top = false
+++

```
@inproceedings{batsim_jsspp16,
  title = {{Batsim: a Realistic Language-Independent Resources and Jobs Management Systems Simulator}},
  author = {Dutot, Pierre-Fran{\c c}ois and Mercier, Michael and Poquet, Millian and Richard, Olivier},
  booktitle = {{20th Workshop on Job Scheduling Strategies for Parallel Processing}},
  address = {Chicago, United States},
  year = {2016},
  month = May,
  keywords = {Reproducibility ; RJMS ; Scheduling ; Simulation},
  url = {https://hal.archives-ouvertes.fr/hal-01333471},
  pdf = {https://hal.archives-ouvertes.fr/hal-01333471/file/batsim.pdf},
  doi = "10.1007/978-3-319-61756-5\{_}10"
}
```
