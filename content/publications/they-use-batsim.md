+++
title = "They use Batsim"
description = "List of Batsim-related publications."
date = 2021-05-25T18:20:00+00:00
updated = 2021-05-25T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
[[extra.list]]
title = "Towards Energy Budget Control in HPC"
booktitle = "Proceedings of the 17th {IEEE/ACM} International Symposium on Cluster, Cloud and Grid Computing, {CCGRID} 2017, Madrid, Spain, May 14-17, 2017"
type = ""
pages = "381--390"
publisher = "{IEEE} Computer Society / {ACM}"
year = "2017"
url = "https://doi.org/10.1109/CCGRID.2017.16"
doi = "10.1109/CCGRID.2017.16"
timestamp = "Wed, 16 Oct 2019 14:14:53 +0200"
biburl = "https://dblp.org/rec/conf/ccgrid/DutotGGLPR17.bib"
bibsource = "dblp computer science bibliography, https://dblp.org"
author = "Pierre-François Dutot, Yiannis Georgiou, David Glesser, Laurent Lefevre, Millian Poquet, Issam Rais"

[[extra.list]]
title = "Batsim: A Realistic Language-Independent Resources and Jobs Management Systems Simulator"
booktitle = "Job Scheduling Strategies for Parallel Processing - 19th and 20th International Workshops, {JSSPP} 2015, Hyderabad, India, May 26, 2015 and {JSSPP} 2016, Chicago, IL, USA, May 27, 2016, Revised Selected Papers"
series = "Lecture Notes in Computer Science"
volume = "10353"
pages = "178--197"
publisher = "Springer"
year = "2016"
url = "https://doi.org/10.1007/978-3-319-61756-5_10"
doi = "10.1007/978-3-319-61756-5_10"
timestamp = "Tue, 14 May 2019 10:00:50 +0200"
biburl = "https://dblp.org/rec/conf/jsspp/DutotMPR16.bib"
bibsource = "dblp computer science bibliography, https://dblp.org"
author = "Pierre-François Dutot, Michael Mercier, Millian Poquet, Olivier Richard"

+++

# Publication list

