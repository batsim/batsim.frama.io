
+++
title = "Contributing"
draft = false
+++

The contribution guidelines are described [here](https://batsim.readthedocs.io/en/latest/contributing.html).
