+++
title = "Contact us"
draft = false
+++

The main way to chat with the Batsim team is on [Batsim’s mattermost](https://framateam.org/signup_user_complete/?id=5xb995hph3d79yj738pokxrnuh).

Otherwise, the <a class="reference external" href="mailto:batsim-user%40inria.fr">batsim-user<span>@</span>inria<span>.</span>fr</a> mailing list allows to ask questions and to get announcements. Registration is done on <a href="https://sympa.inria.fr/sympa/info/batsim-user">here</a>.