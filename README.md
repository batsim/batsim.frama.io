# batsim.frama.io
Batsim's website, as hosted on https://batsim.frama.io or https://batsim.org.

## Work from a development shell (recommended)

You can enter a development shell with this command:

```
nix-shell default.nix -A devShell
```

You can then preview the site with this command:

```
zola serve
# ...
# Web server is available at http://127.0.0.1:1111
```

## Build the website

This is not recommended, as zola does not generate a site that can be browsed without http server.

You can still do it with this command:

```
nix-build default.nix -A packages.x86_64-linux.batsite
```

Or, alternatively if you have Nix flakes enabled:

```
nix build .#batsite
```

You can also do in from a shell:
```
nix-shell default.nix -A devShell
# then, into the new shell:
zola build -o /tmp/batsite
```
