{
  inputs = {
    oxalica.url = "github:oxalica/rust-overlay";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, oxalica }:
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [ oxalica.overlay ];
        config.allowUnfree = true;
      };
    in {

      packages.x86_64-linux = {
        batsite = pkgs.stdenv.mkDerivation rec {
          version = "0.0.1";
          name = "batsite-${version}";
          src = pkgs.lib.sourceByRegex ./. [
            "^content"
            "^content/.*\.md"
            "^content/publications"
            "^content/publications/.*\.md"
            "^static"
            "^static/.*"
            "^scripts"
            "^scripts/.*\.py"
            "^templates"
            "^templates/.*"
            "^themes"
            "^themes/.*"
            "publications.bib"
            "config.toml"
          ];
          base-url = "https://batsim.frama.io";
          buildInputs = with pkgs; [
            zola
            pandoc
            python3
            python3Packages.setuptools
            python3Packages.bibtexparser
            python3Packages.toml
          ];
          checkPhase = ''
            zola check
          '';
          installPhase = ''
            python3 scripts/generate-publications-md.py > content/publications/they-use-batsim.md
            zola build -o $out --base-url ${base-url}
          '';
        };
      };
      devShell.x86_64-linux = pkgs.mkShell {
        buildInputs = with pkgs; [
          zola
          pandoc
          python3
          python3Packages.bibtexparser
          python3Packages.setuptools
          python3Packages.toml
        ];
        shellHook = "zsh; exit 0";
      };
    };
}
